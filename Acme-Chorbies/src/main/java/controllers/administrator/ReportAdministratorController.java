
package controllers.administrator;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.ChorbiService;
import controllers.AbstractController;

@Controller
@RequestMapping("/report/administrator")
public class ReportAdministratorController extends AbstractController {

	// Services ---------------------------------------

	@Autowired
	private ActorService	actorService;

	@Autowired
	private ChorbiService	chorbiService;


	// Constructors -----------------------------------

	public ReportAdministratorController() {
		super();
	}

	// view -----------------------------------------
	@RequestMapping(value = "/dashboard", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView result;

		//C queries

		//Query 1
		final Collection<Object> cfc = this.chorbiService.listNumberChorbiesForCity();
		final Collection<Object> cfc2 = this.chorbiService.listNumberChorbiesForCountry();
		//Query 2
		final Collection<Object> aoc = this.chorbiService.calculateMinMaxAvgAgesOfChorbies();
		//Query 3
		final Collection<Object> nvcc = this.chorbiService.calculateRatioOfChorbiesWhoHaveNotCreditCard();
		//Query 4
		final Collection<Object> rca = this.chorbiService.calculateRatioOfChorbiesWhoSearchActivities();
		final Collection<Object> rcf = this.chorbiService.calculateRatioOfChorbiesWhoSearchFriendShip();
		final Collection<Object> rcl = this.chorbiService.calculateRatioOfChorbiesWhoSearchLove();

		//B queries
		//Query 1
		final Collection<Object> csl = this.chorbiService.listChorbiesSortedLikes();
		//Query 2
		final Collection<Object> mal = this.chorbiService.calculateMinMaxAvgLikesOfChorbies();

		//A queries
		//Query 1
		final Collection<Object> mavcr = this.chorbiService.minMaxAvgChirpRevieced();

		//Query 2
		final Collection<Object> mavcs = this.chorbiService.minMaxAvgChirpSent();
		//Query 3
		final Collection<Object> crmc = this.chorbiService.findChorbiesWhoReceivedMoreChips();

		//Query 4
		final Collection<Object> csmc = this.chorbiService.findChorbiesWhoSentMoreChips();

		//C queries

		result = new ModelAndView("report/dashboard");
		//Query 1
		result.addObject("cfc", cfc);
		result.addObject("cfc2", cfc2);
		//Query 2
		result.addObject("aoc", aoc);
		//Query 3
		result.addObject("nvcc", nvcc);
		//Query 4
		result.addObject("rca", rca);
		result.addObject("rcf", rcf);
		result.addObject("rcl", rcl);

		//B queries

		result.addObject("csl", csl);
		result.addObject("mal", mal);

		//A Queries
		result.addObject("mavcr", mavcr);
		result.addObject("mavcs", mavcs);
		result.addObject("crmc", crmc);
		result.addObject("csmc", csmc);
		return result;
	}
}
