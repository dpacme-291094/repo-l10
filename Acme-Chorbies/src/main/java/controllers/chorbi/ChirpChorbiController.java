
package controllers.chorbi;

import java.util.Collection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ChirpService;
import services.ChorbiService;
import controllers.AbstractController;
import domain.Chirp;
import domain.Chorbi;
import forms.FormChirp;

@Controller
@RequestMapping("/chirp/chorbi")
public class ChirpChorbiController extends AbstractController {

	// Services ---------------------------------------------------------------
	@Autowired
	private ChirpService	chirpService;

	@Autowired
	private ChorbiService	chorbiService;


	// Constructors -----------------------------------------------------------

	public ChirpChorbiController() {
		super();
	}

	// Listing ----------------------------------------------------------------
	@RequestMapping(value = "/inbox", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView result;
		Collection<Chirp> chirps;
		final Chorbi a = this.chorbiService.findByPrincipal();

		chirps = this.chirpService.findByRecipient(a);

		result = new ModelAndView("chirp/inbox");

		result.addObject("chirps", chirps);
		result.addObject("whereIAm", "i");
		result.addObject("ruta", "chirp/chorbi/inbox.do");

		return result;
	}

	@RequestMapping(value = "/outbox", method = RequestMethod.GET)
	public ModelAndView list2() {
		ModelAndView result;
		Collection<Chirp> chirps;
		final Chorbi a = this.chorbiService.findByPrincipal();

		chirps = this.chirpService.findBySender(a);

		result = new ModelAndView("chirp/outbox");

		result.addObject("chirps", chirps);
		result.addObject("whereIAm", "o");
		result.addObject("ruta", "chirp/chorbi/outbox.do");
		return result;
	}

	// Read -------------------------------------------------------------------

	@RequestMapping(value = "/read", method = RequestMethod.GET)
	public ModelAndView read(@RequestParam final int id, @RequestParam final String l) {
		ModelAndView result;
		Chirp m2;
		if (this.chirpService.findOne(id) == null || (!this.chorbiService.findByPrincipal().equals(this.chirpService.findOne(id).getChirpRecipient()) && !this.chorbiService.findByPrincipal().equals(this.chirpService.findOne(id).getChirpSender())))
			throw new RuntimeException("Oops! You should not be here. Go back to home page.");
		else {
			final Chorbi me = this.chorbiService.findByPrincipal();
			if (id != 0
				&& this.chirpService.findOne(id) != null
				&& ((this.chirpService.findOne(id).getChirpRecipient() != null && this.chirpService.findOne(id).getChirpRecipient().equals(me)) || (this.chirpService.findOne(id).getChirpSender() != null && this.chirpService.findOne(id).getChirpSender()
					.equals(me)))) {
				m2 = this.chirpService.findOne(id);
				final FormChirp formChirp = new FormChirp(m2);

				result = new ModelAndView("chirp/edit");

				result.addObject("formChirp", formChirp);
				result.addObject("sender", m2.getChirpSender());
				result.addObject("recipient", m2.getChirpRecipient());
				String ruta = "/chirp/chorbi/inbox.do";
				if (l.equals("o"))
					ruta = "/chirp/chorbi/outbox.do";
				result.addObject("ruta", ruta);
			} else {
				String aux = "/chirp/chorbi/inbox.do";
				if (l.equals("o"))
					aux = "/chirp/chorbi/outbox.do";
				result = new ModelAndView(aux);
			}
		}
		return result;
	}
	// Create -----------------------------------------------------------------

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ModelAndView create(@RequestParam(defaultValue = "i") final String l, @RequestParam() final String id) {
		ModelAndView result;
		final Chirp m2 = this.chirpService.create();
		final FormChirp formChirp = new FormChirp(m2);
		formChirp.setUserRecipient(this.chorbiService.findByPrincipal().getUserAccount().getUsername());
		formChirp.setUserRecipient(this.chorbiService.findOne(Integer.parseInt(id)).getUserAccount().getUsername());
		final String ruta = "/chorbies/chorbi/profile.do?chorbiId=" + id;

		result = new ModelAndView("chirp/edit");
		result.addObject("ruta", ruta);
		result.addObject("formChirp", formChirp);
		result.addObject("create", true);

		return result;
	}

	// Forward ----------------------------------------------------------------

	@RequestMapping(value = "/send", method = RequestMethod.POST, params = "forward")
	public ModelAndView forward(@RequestParam final int id, @RequestParam final String whereIAm) {
		ModelAndView result;
		final Chorbi me = this.chorbiService.findByPrincipal();

		final Chirp m2 = this.chirpService.findOne(id);
		final FormChirp forwarder = new FormChirp(m2);
		if ((m2.getChirpRecipient() != null && m2.getChirpRecipient().equals(me)) || (m2.getChirpSender() != null && m2.getChirpSender().equals(me))) {
			final Chirp formChirp = this.chirpService.create();
			formChirp.setAttachments(forwarder.getAttachments());
			formChirp.setText(forwarder.getText());
			formChirp.setSubject(forwarder.getSubject());
			formChirp.setUserSender(me.getUserAccount().getUsername());
			String ruta = "/chirp/chorbi/inbox.do";
			if (whereIAm.equals("o"))
				ruta = "/chirp/chorbi/outbox.do";

			result = new ModelAndView("chirp/edit");
			result.addObject("ruta", ruta);
			result.addObject("formChirp", formChirp);
			result.addObject("create", true);
		} else {
			String aux = "/chirp/chorbi/inbox.do";
			if (whereIAm.equals("o"))
				aux = "/chirp/chorbi/outbox.do";
			result = new ModelAndView(aux);
		}

		return result;
	}

	// Reply ----------------------------------------------------------------

	@RequestMapping(value = "/send", method = RequestMethod.POST, params = "reply")
	public ModelAndView reply(@RequestParam final int id, @RequestParam final String whereIAm) {
		ModelAndView result;
		final Chorbi me = this.chorbiService.findByPrincipal();

		final Chirp m2 = this.chirpService.findOne(id);
		final FormChirp replicant = new FormChirp(m2);
		if ((m2.getChirpRecipient() != null && m2.getChirpRecipient().equals(me)) || (m2.getChirpSender() != null && m2.getChirpSender().equals(me))) {
			final Chirp formChirp = this.chirpService.create();
			formChirp.setChirpRecipient(m2.getChirpSender());
			formChirp.setSubject("RE: " + replicant.getSubject());
			formChirp.setUserRecipient(replicant.getUserSender());
			formChirp.setUserSender(replicant.getUserRecipient());
			String ruta = "/chirp/chorbi/inbox.do";
			if (whereIAm.equals("o"))
				ruta = "/chirp/chorbi/outbox.do";

			result = new ModelAndView("chirp/edit");
			result.addObject("ruta", ruta);
			result.addObject("formChirp", formChirp);
			result.addObject("create", true);
		} else {
			String aux = "/chirp/chorbi/inbox.do";
			if (whereIAm.equals("o"))
				aux = "/chirp/chorbi/outbox.do";
			result = new ModelAndView(aux);
		}

		return result;
	}

	// Delete ----------------------------------------------------------------

	@RequestMapping(value = "/delete", method = RequestMethod.POST, params = "delete")
	public ModelAndView delete(@RequestParam final int id, @RequestParam final String whereIAm) throws Exception {
		ModelAndView result;
		Collection<Chirp> chirps;
		final Chorbi me = this.chorbiService.findByPrincipal();
		if (whereIAm.equals("i")) {
			result = new ModelAndView("chirp/inbox");
			chirps = this.chirpService.findByRecipient(me);
		} else {
			result = new ModelAndView("chirp/outbox");
			chirps = this.chirpService.findBySender(me);

		}

		final String error = "chirp.commit.error";

		try {
			final Chirp m = this.chirpService.findOne(id);

			if ((m.getChirpRecipient() != null && m.getChirpRecipient().equals(me)) || (m.getChirpSender() != null && m.getChirpSender().equals(me))) {
				if (whereIAm.equals("i"))
					this.chirpService.delete(m, false);
				else
					this.chirpService.delete(m, true);
				chirps.remove(m);
				result.addObject("chirps", chirps);
				result.addObject("whereIAm", whereIAm);
				result.addObject("message1", "chirp.commit.ok");

			} else {

				result.addObject("chirps", chirps);
				result.addObject("whereIAm", whereIAm);
				result.addObject("message", error);
			}

		} catch (final Throwable oops) {
			result.addObject("chirps", chirps);
			result.addObject("whereIAm", whereIAm);
			result.addObject("message", error);
		}

		return result;
	}

	// Send ----------------------------------------------------------------

	@RequestMapping(value = "/send", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@ModelAttribute("formChirp") final FormChirp formChirp, final BindingResult binding) {
		ModelAndView result;
		final Chirp mensaje = this.chirpService.reconstruct(formChirp, binding);
		if (binding.hasErrors()) {
			result = new ModelAndView("chirp/edit");

			result.addObject("formChirp", formChirp);
			result.addObject("create", true);
			result.addObject("ruta", "/chirp/chorbi/outbox.do");
			result.addObject("message", null);
		} else {
			Chorbi recipient;
			boolean fine = true;
			String error = "";
			if (mensaje.getAttachments().length() != 0) {
				final String[] urls = mensaje.getAttachments().split(",");
				for (final String url : urls)
					if (!ChirpChorbiController.isUrl(url)) {
						fine = false;
						error = "chirp.error.url";
						break;
					}
			}
			recipient = this.chorbiService.findByUsername(mensaje.getUserRecipient());
			if (recipient == null) {
				fine = false;
				error = "chirp.actor.notfound";
			} else if (recipient.equals(this.chorbiService.findByPrincipal())) {
				fine = false;
				error = "chirp.actor.notme";
			}
			if (!fine) {
				result = new ModelAndView("chirp/edit");

				result.addObject("formChirp", formChirp);
				result.addObject("create", true);
				result.addObject("ruta", "/chirp/chorbi/inbox.do");
				result.addObject("message", error);
			} else {
				this.chirpService.save(mensaje);

				result = new ModelAndView("redirect:../chorbi/outbox.do");

				result.addObject("message1", "message.commit.ok");

			}
		}

		return result;
	}

	private static boolean isUrl(final String s) {
		final String regex = "(https?:\\/\\/(?:www\\.)[^\\s\\.]+\\.[^\\s]{2,}|ftp?:\\/\\/[^\\s\\.]+\\.[^\\s]{2,}|http?:\\/\\/[^\\s\\.]+\\.[^\\s]{2,})";

		try {
			final Pattern patt = Pattern.compile(regex);
			final Matcher matcher = patt.matcher(s);
			return matcher.matches();

		} catch (final RuntimeException e) {
			return false;
		}
	}
}
