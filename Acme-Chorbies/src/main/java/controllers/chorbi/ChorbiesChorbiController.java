
package controllers.chorbi;

import java.util.Collection;
import java.util.HashSet;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.ChorbiService;
import services.LikesService;
import controllers.AbstractController;
import domain.Actor;
import domain.Chorbi;
import domain.Likes;

@Controller
@RequestMapping("/chorbies/chorbi")
public class ChorbiesChorbiController extends AbstractController {

	// Services ---------------------------------------------------------------
	@Autowired
	private ChorbiService	chorbiService;
	@Autowired
	private ActorService	actorService;
	@Autowired
	private LikesService	likeService;


	// Constructor ---------------------------------------------------------------
	public ChorbiesChorbiController() {
		super();
	}

	// Create -------------------------------------

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {

		ModelAndView result;
		final Actor a = this.actorService.findByPrincipal();

		if (a instanceof Chorbi) {
			final Chorbi c = this.chorbiService.findByPrincipal();
			final Collection<Chorbi> chorbies = this.chorbiService.findAll();
			chorbies.remove(c);
			result = new ModelAndView("chorbi/list");
			result.addObject("chorbies", chorbies);
			result.addObject("uri", "chorbies/chorbi/list.do");
			result.addObject("isAdmin", false);

		} else {
			final Collection<Chorbi> chorbies = this.chorbiService.findAll();
			result = new ModelAndView("chorbi/list");
			result.addObject("chorbies", chorbies);
			result.addObject("uri", "chorbies/chorbi/list.do");
			result.addObject("isAdmin", true);
		}

		return result;

	}
	@RequestMapping(value = "/profile", method = RequestMethod.GET)
	public ModelAndView profile(@Valid final int chorbiId) {
		ModelAndView result;

		final Chorbi chorbi = this.chorbiService.findOne(chorbiId);

		if (chorbi == null)
			throw new RuntimeException("Oops! You should not be here. Go back to home page.");
		else {

			result = new ModelAndView("chorbi/profile");

			result.addObject("chorbi", chorbi);
			result.addObject("isChorbi", true);

			if (chorbi.getId() == this.chorbiService.findByPrincipal().getId())
				result.addObject("myself", true);
			else
				result.addObject("myself", false);
			if (this.chorbiService.findByPrincipal().getLikesSent() != null && !this.chorbiService.findByPrincipal().getLikesSent().isEmpty())
				for (final Likes a : this.chorbiService.findByPrincipal().getLikesSent())
					if (a.getLikesRecipient().getId() == chorbi.getId()) {
						result.addObject("following", true);
						result.addObject("likeId", a.getId());
						break;
					} else
						result.addObject("following", false);
			else
				result.addObject("following", false);
			result.addObject("age", ChorbiService.getAge(chorbi.getBirthDate()));
		}
		return result;
	}

	@RequestMapping(value = "/profile", method = RequestMethod.POST, params = "like")
	public ModelAndView like(@Valid final Integer chorbiId) {

		ModelAndView result;

		result = new ModelAndView("like/edit");
		result.addObject("chorbi", this.chorbiService.findOne(chorbiId));
		result.addObject("like", this.likeService.create(this.chorbiService.findOne(chorbiId)));
		return result;
	}

	@RequestMapping(value = "/profile", method = RequestMethod.POST, params = "dislike")
	public ModelAndView dislike(@Valid final Integer likeId) {

		final ModelAndView result;

		final Likes l = this.likeService.findOne(likeId);
		final Chorbi recipient = l.getLikesRecipient();
		this.likeService.dislike(l);
		result = new ModelAndView("redirect: ../../../../chorbies/chorbi/profile.do?chorbiId=" + recipient.getId());

		return result;
	}
	@RequestMapping(value = "/likesList", method = RequestMethod.GET)
	public ModelAndView likesList(@Valid final int chorbiId) {
		final Actor a = this.actorService.findByPrincipal();
		ModelAndView result;
		final Chorbi c = this.chorbiService.findOne(chorbiId);
		if (c == null)
			throw new RuntimeException("Oops! You should not be here. Go back to home page.");
		else {

			final Collection<Likes> likes = c.getLikesReceived();
			final Collection<Chorbi> chorbies = new HashSet<Chorbi>();
			for (final Likes l : likes)
				chorbies.add(l.getLikesSender());

			result = new ModelAndView("chorbi/list");
			result.addObject("chorbies", likes);
			result.addObject("isLiker", true);
			result.addObject("uri", "chorbies/chorbi/likesList.do");
			if (a instanceof Chorbi)
				result.addObject("isAdmin", false);
			else
				result.addObject("isAdmin", true);
		}
		return result;

	}
}
