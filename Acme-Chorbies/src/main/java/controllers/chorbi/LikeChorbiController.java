
package controllers.chorbi;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.ChorbiService;
import services.LikesService;
import controllers.AbstractController;
import domain.Likes;

@Controller
@RequestMapping("/like/chorbi")
public class LikeChorbiController extends AbstractController {

	// Services ---------------------------------------------------------------
	@Autowired
	private ChorbiService	chorbiService;
	@Autowired
	private ActorService	actorService;
	@Autowired
	private LikesService	likeService;


	// Constructor ---------------------------------------------------------------
	public LikeChorbiController() {
		super();
	}

	// Create -------------------------------------

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final Likes like, final BindingResult binding) {

		ModelAndView result;

		if (binding.hasErrors())
			result = this.createEditModelAndView(like);
		else
			try {
				final Likes k = this.likeService.save(like);
				this.likeService.like(like.getLikesRecipient(), k);
				result = new ModelAndView("redirect: ../../../../chorbies/chorbi/profile.do?chorbiId=" + like.getLikesRecipient().getId());

			} catch (final Throwable oops) {
				result = this.createEditModelAndView(like, "like.error");
			}
		return result;
	}
	protected ModelAndView createEditModelAndView(final Likes like) {
		ModelAndView result;

		result = this.createEditModelAndView(like, null);

		return result;
	}

	protected ModelAndView createEditModelAndView(final Likes like, final String message) {
		ModelAndView result;

		result = new ModelAndView("like/edit");
		result.addObject("like", like);
		result.addObject("message", message);
		return result;
	}

}
