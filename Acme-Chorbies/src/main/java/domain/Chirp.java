
package domain;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.Past;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.SafeHtml.WhiteListType;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Access(AccessType.PROPERTY)
@Table(indexes = {
	@Index(columnList = "moment")
})
public class Chirp extends DomainEntity {

	// Constructors -----------------------------------------------------------

	public Chirp() {
		super();
	}


	// Attributes -------------------------------------------------------------

	private Date	moment;
	private String	subject;
	private String	text;
	private String	attachments;
	private String	userSender;
	private String	userRecipient;


	@Past
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	public Date getMoment() {
		return this.moment;
	}

	public void setMoment(final Date moment) {
		this.moment = moment;
	}
	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getSubject() {
		return this.subject;
	}

	public void setSubject(final String subject) {
		this.subject = subject;
	}
	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getText() {
		return this.text;
	}

	public void setText(final String text) {
		this.text = text;
	}

	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getAttachments() {
		return this.attachments;
	}

	public void setAttachments(final String attachments) {
		this.attachments = attachments;
	}

	@NotBlank
	public String getUserSender() {
		return this.userSender;
	}

	public void setUserSender(final String userSender) {
		this.userSender = userSender;
	}

	@NotBlank
	public String getUserRecipient() {
		return this.userRecipient;
	}

	public void setUserRecipient(final String userRecipient) {
		this.userRecipient = userRecipient;
	}


	private Chorbi	chirpSender;
	private Chorbi	chirpRecipient;


	@Valid
	@ManyToOne(optional = true)
	public Chorbi getChirpSender() {
		return this.chirpSender;
	}

	public void setChirpSender(final Chorbi chirpSender) {
		this.chirpSender = chirpSender;
	}

	@Valid
	@ManyToOne(optional = true)
	public Chorbi getChirpRecipient() {
		return this.chirpRecipient;
	}

	public void setChirpRecipient(final Chorbi chirpRecipient) {
		this.chirpRecipient = chirpRecipient;
	}

}
