
package domain;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.SafeHtml.WhiteListType;
import org.hibernate.validator.constraints.URL;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Access(AccessType.PROPERTY)
@Table(indexes = {
	@Index(columnList = "description, relationship, banned, birthDate, genre, city, country, state, province")
})
public class Chorbi extends Actor {

	// Constructors -----------------------------------------------------------

	public Chorbi() {
		super();
	}


	// Attributes -------------------------------------------------------------

	private String		picture;
	private String		description;
	private String		relationship;
	private Date		birthDate;
	private String		genre;
	private Coordinates	coordinates;
	private CreditCard	creditCard;
	private boolean		banned;


	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	@URL
	public String getPicture() {
		return this.picture;
	}

	public void setPicture(final String picture) {
		this.picture = picture;
	}

	@SafeHtml(whitelistType = WhiteListType.NONE)
	@NotBlank
	public String getDescription() {
		return this.description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}
	@NotBlank
	@Pattern(regexp = "^(activities|friendship|love)$")
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getRelationship() {
		return this.relationship;
	}

	public void setRelationship(final String relationship) {
		this.relationship = relationship;
	}
	@NotNull
	@Past
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	public Date getBirthDate() {
		return this.birthDate;
	}

	public void setBirthDate(final Date birthDate) {
		this.birthDate = birthDate;
	}
	@NotBlank
	@Pattern(regexp = "^(man|woman)$")
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getGenre() {
		return this.genre;
	}

	public void setGenre(final String genre) {
		this.genre = genre;
	}
	@Valid
	@NotNull
	public Coordinates getCoordinates() {
		return this.coordinates;
	}

	public void setCoordinates(final Coordinates coordinates) {
		this.coordinates = coordinates;
	}

	public CreditCard getCreditCard() {
		return this.creditCard;
	}

	public void setCreditCard(final CreditCard creditCard) {
		this.creditCard = creditCard;
	}

	public boolean isBanned() {
		return this.banned;
	}

	public void setBanned(final boolean banned) {
		this.banned = banned;
	}


	//Relationships ----------------------------------------------------
	private Collection<Likes>	likesReceived;
	private Collection<Likes>	likesSent;
	private Collection<Chirp>	chirpsReceived;
	private Collection<Chirp>	chirpsSent;
	private SearchTemplate		search;


	@Valid
	@NotNull
	@OneToMany(mappedBy = "likesRecipient")
	public Collection<Likes> getLikesReceived() {
		return this.likesReceived;
	}

	public void setLikesReceived(final Collection<Likes> likesReceived) {
		this.likesReceived = likesReceived;
	}
	@Valid
	@NotNull
	@OneToMany(mappedBy = "likesSender")
	public Collection<Likes> getLikesSent() {
		return this.likesSent;
	}

	public void setLikesSent(final Collection<Likes> likesSent) {
		this.likesSent = likesSent;
	}
	@Valid
	@NotNull
	@OneToMany(mappedBy = "chirpRecipient")
	public Collection<Chirp> getChirpsReceived() {
		return this.chirpsReceived;
	}

	public void setChirpsReceived(final Collection<Chirp> chirpsReceived) {
		this.chirpsReceived = chirpsReceived;
	}
	@Valid
	@NotNull
	@OneToMany(mappedBy = "chirpSender")
	public Collection<Chirp> getChirpsSent() {
		return this.chirpsSent;
	}

	public void setChirpsSent(final Collection<Chirp> chirpsSent) {
		this.chirpsSent = chirpsSent;
	}

	@OneToOne(mappedBy = "chorbi")
	public SearchTemplate getSearch() {
		return this.search;
	}

	public void setSearch(final SearchTemplate search) {
		this.search = search;
	}

}
