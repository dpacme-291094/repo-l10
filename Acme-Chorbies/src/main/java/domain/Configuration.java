
package domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.SafeHtml.WhiteListType;

@Entity
@Access(AccessType.PROPERTY)
public class Configuration extends DomainEntity {

	// Constructors -----------------------------------------------------------

	public Configuration() {
		super();
	}


	// Attributes -------------------------------------------------------------

	private String	banners;
	private String	searchHours;


	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getBanners() {
		return this.banners;
	}

	public void setBanners(final String banners) {
		this.banners = banners;
	}

	@NotBlank
	@Pattern(regexp = "^([0-9]{2}):([0-9]{2}):([0-9]{2})$")
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getSearchHours() {
		return this.searchHours;
	}

	public void setSearchHours(final String searchHours) {
		this.searchHours = searchHours;
	}

	// Relationships ----------------------------------------------------------

}
