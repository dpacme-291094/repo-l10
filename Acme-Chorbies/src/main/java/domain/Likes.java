
package domain;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.SafeHtml.WhiteListType;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Access(AccessType.PROPERTY)
public class Likes extends DomainEntity {

	// Constructors -----------------------------------------------------------

	public Likes() {
		super();
	}


	// Attributes -------------------------------------------------------------

	private Date	moment;
	private String	comment;


	@NotNull
	@Past
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	public Date getMoment() {
		return this.moment;
	}

	public void setMoment(final Date moment) {
		this.moment = moment;
	}

	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getComment() {
		return this.comment;
	}

	public void setComment(final String comment) {
		this.comment = comment;
	}


	private Chorbi	likesSender;
	private Chorbi	likesRecipient;


	@Valid
	@NotNull
	@ManyToOne(optional = false)
	public Chorbi getLikesSender() {
		return this.likesSender;
	}

	public void setLikesSender(final Chorbi likesSender) {
		this.likesSender = likesSender;
	}

	@Valid
	@NotNull
	@ManyToOne(optional = false)
	public Chorbi getLikesRecipient() {
		return this.likesRecipient;
	}

	public void setLikesRecipient(final Chorbi likesRecipient) {
		this.likesRecipient = likesRecipient;
	}

}
