
package forms;

import java.util.Collection;
import java.util.Date;

import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.SafeHtml.WhiteListType;
import org.hibernate.validator.constraints.URL;
import org.springframework.format.annotation.DateTimeFormat;

import security.UserAccount;
import domain.Chirp;
import domain.Coordinates;
import domain.Likes;
import domain.SearchTemplate;

public class FormChorbi {

	// *ToDel* Creo que aqu� no va la CreditCard, puesto que no se va usar para el register.
	private int					id;
	private int					version;
	private UserAccount			userAccount;
	private String				name;
	private String				surname;
	private String				phone;
	private String				email;
	private String				genre;
	private Date				birthDate;
	private String				picture;
	private String				relationship;
	private boolean				banned;
	private String				description;
	private Coordinates			coordinates;
	private Collection<Likes>	likesReceived;
	private Collection<Likes>	likesSent;
	private Collection<Chirp>	chirpsReceived;
	private Collection<Chirp>	chirpsSent;
	private SearchTemplate		search;


	@SafeHtml(whitelistType = WhiteListType.NONE)
	public int getId() {
		return this.id;
	}

	public void setId(final int id) {
		this.id = id;
	}

	@SafeHtml(whitelistType = WhiteListType.NONE)
	public int getVersion() {
		return this.version;
	}

	public void setVersion(final int version) {
		this.version = version;
	}

	public UserAccount getUserAccount() {
		return this.userAccount;
	}

	public void setUserAccount(final UserAccount userAccount) {
		this.userAccount = userAccount;
	}

	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getName() {
		return this.name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getSurname() {
		return this.surname;
	}

	public void setSurname(final String surname) {
		this.surname = surname;
	}

	@Email
	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getEmail() {
		return this.email;
	}

	public void setEmail(final String email) {
		this.email = email;
	}

	@NotBlank
	@Pattern(regexp = "^(([+])([0-9]{1,3})([ ])?)?(([0-9]{3}([ ])?[0-9]{3}([ ])?[0-9]{3})|([0-9]{3}([ ])?[0-9]{2}([ ])?[0-9]{2}([ ])?[0-9]{2}))$$")
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getPhone() {
		return this.phone;
	}

	public void setPhone(final String phone) {
		this.phone = phone;
	}

	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	@URL
	public String getPicture() {
		return this.picture;
	}

	public void setPicture(final String picture) {
		this.picture = picture;
	}

	@SafeHtml(whitelistType = WhiteListType.NONE)
	@NotBlank
	public String getDescription() {
		return this.description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}
	@NotBlank
	@Pattern(regexp = "^(activities|friendship|love)$")
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getRelationship() {
		return this.relationship;
	}

	public void setRelationship(final String relationship) {
		this.relationship = relationship;
	}
	@NotNull
	@Past
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	public Date getBirthDate() {
		return this.birthDate;
	}

	public void setBirthDate(final Date birthDate) {
		this.birthDate = birthDate;
	}

	@NotBlank
	@Pattern(regexp = "^(man|woman)$")
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getGenre() {
		return this.genre;
	}

	public void setGenre(final String genre) {
		this.genre = genre;
	}

	@Valid
	@NotNull
	public Coordinates getCoordinates() {
		return this.coordinates;
	}

	public void setCoordinates(final Coordinates coordinates) {
		this.coordinates = coordinates;
	}

	public boolean isBanned() {
		return this.banned;
	}

	public void setBanned(final boolean banned) {
		this.banned = banned;
	}

	@Valid
	@NotNull
	@OneToMany(mappedBy = "likesRecipient")
	public Collection<Likes> getLikesReceived() {
		return this.likesReceived;
	}

	public void setLikesReceived(final Collection<Likes> likesReceived) {
		this.likesReceived = likesReceived;
	}

	@Valid
	@NotNull
	@OneToMany(mappedBy = "likesSender")
	public Collection<Likes> getLikesSent() {
		return this.likesSent;
	}

	public void setLikesSent(final Collection<Likes> likesSent) {
		this.likesSent = likesSent;
	}

	@Valid
	@NotNull
	@OneToMany(mappedBy = "chirpRecipient")
	public Collection<Chirp> getChirpsReceived() {
		return this.chirpsReceived;
	}

	public void setChirpsReceived(final Collection<Chirp> chirpsReceived) {
		this.chirpsReceived = chirpsReceived;
	}

	@Valid
	@NotNull
	@OneToMany(mappedBy = "chirpSender")
	public Collection<Chirp> getChirpsSent() {
		return this.chirpsSent;
	}

	public void setChirpsSent(final Collection<Chirp> chirpsSent) {
		this.chirpsSent = chirpsSent;
	}

	@OneToOne(mappedBy = "chorbi")
	public SearchTemplate getSearch() {
		return this.search;
	}

	public void setSearch(final SearchTemplate search) {
		this.search = search;
	}
}
