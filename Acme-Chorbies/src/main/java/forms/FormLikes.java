
package forms;

import java.util.Date;

import javax.validation.constraints.Past;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.SafeHtml.WhiteListType;
import org.springframework.format.annotation.DateTimeFormat;

import domain.Likes;

public class FormLikes {

	// Attributes -------------------------------------------------------------
	private Date	moment;
	private String	comment;


	public FormLikes() {
		super();
	}

	public FormLikes(final Likes m) {

		final Date currentDate = new Date();
		currentDate.setTime(currentDate.getTime() - 60000);
		this.setMoment(currentDate);
		this.setComment(m.getComment());

	}

	@Past
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	public Date getMoment() {
		return this.moment;
	}

	public void setMoment(final Date moment) {
		this.moment = moment;
	}

	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getComment() {
		return this.comment;
	}

	public void setComment(final String comment) {
		this.comment = comment;
	}

}
