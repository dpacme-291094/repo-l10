
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.ChirpRepository;
import domain.Chirp;
import domain.Chorbi;
import forms.FormChirp;

@Service
@Transactional
public class ChirpService {

	// Managed repository -------------------------------
	@Autowired
	private ChirpRepository	chirpRepository;

	@Autowired
	private ChorbiService	chorbiService;

	@Autowired
	private Validator		validator;


	// Constructor --------------------------------------
	public ChirpService() {
		super();
	}

	// Simple CRUD methods ------------------------------
	public Chirp create() {

		final Chirp m = new Chirp();
		final Chorbi sender = this.chorbiService.findByPrincipal();
		m.setText("");
		m.setSubject("");

		m.setUserSender(sender.getUserAccount().getUsername());
		m.setChirpSender(sender);
		final Date currentDate = new Date();
		currentDate.setTime(currentDate.getTime() - 600000);
		m.setMoment(currentDate);

		return m;
	}

	public Chirp save(final Chirp mm) {
		Chirp m = null;
		if (mm.getId() != 0)
			m = this.chirpRepository.findOne(mm.getId());
		else {
			Assert.isTrue(mm.getId() == 0);
			Assert.notNull(mm);
			if (!mm.getAttachments().isEmpty())
				Assert.isTrue(this.checkUrls(mm.getAttachments()));
			Chorbi recipient = mm.getChirpRecipient();
			if (recipient == null)
				recipient = this.chorbiService.findByUsername(mm.getUserRecipient());
			Assert.notNull(recipient);
			final Date currentDate = new Date();
			currentDate.setTime(currentDate.getTime() - 60000);
			mm.setMoment(currentDate);
			final Collection<Chirp> cm = recipient.getChirpsReceived();
			cm.add(mm);
			recipient.setChirpsReceived(cm);
			this.chorbiService.edit(recipient);
			final Chorbi connected = this.chorbiService.findByPrincipal();
			final Collection<Chirp> cm2 = connected.getChirpsSent();
			mm.setUserSender(connected.getUserAccount().getUsername());
			cm2.add(mm);
			connected.setChirpsSent(cm2);
			this.chorbiService.edit(connected);
			mm.setText(this.mask(mm.getText()));
			mm.setSubject(this.mask(mm.getSubject()));
			m = this.chirpRepository.save(mm);
		}
		return m;
	}

	public void deleteWR(final Chirp m) {
		this.chirpRepository.delete(m.getId());
	}

	public void delete(final Chirp m, final boolean sender) {
		this.checkPrincipal(m);
		final Chorbi connected = this.chorbiService.findByPrincipal();
		if (m.getChirpRecipient() == connected && !sender) {
			final Collection<Chirp> cm = connected.getChirpsReceived();
			cm.remove(m);
			connected.setChirpsReceived(cm);
			this.chorbiService.edit(connected);
			m.setChirpRecipient(null);
		} else if (m.getChirpSender() == connected && sender) {
			final Collection<Chirp> cm = connected.getChirpsSent();
			cm.remove(m);
			connected.setChirpsSent(cm);
			this.chorbiService.edit(connected);
			m.setChirpSender(null);
		}
		if (m.getChirpRecipient() == null && m.getChirpSender() == null)
			this.chirpRepository.delete(m);

	}

	public Collection<Chirp> findBySender(final Chorbi sender) {
		return this.chirpRepository.findBySender(sender.getId());
	}

	public Collection<Chirp> findByRecipient(final Chorbi recipient) {
		return this.chirpRepository.findByRecipient(recipient.getId());
	}

	// Other Chirp methods ---------------------------

	public Collection<Chirp> findByPrincipal() {
		Collection<Chirp> result = new ArrayList<Chirp>();
		final Chorbi chorbi = this.chorbiService.findByPrincipal();

		result = this.chirpRepository.findByChorbi(chorbi);
		return result;
	}

	public Chirp findOne(final int id) {
		return this.chirpRepository.findOne(id);
	}

	public Chirp reconstruct(final FormChirp formChirp, final BindingResult binding) {
		final Chirp result = new Chirp();

		result.setText(formChirp.getText());
		result.setSubject(formChirp.getSubject());
		result.setAttachments(formChirp.getAttachments());
		result.setMoment(formChirp.getMoment());
		result.setChirpRecipient(this.chorbiService.findByUsername(formChirp.getUserRecipient()));
		result.setChirpSender(this.chorbiService.findByUsername(formChirp.getUserSender()));
		result.setUserRecipient(formChirp.getUserRecipient());
		result.setUserSender(formChirp.getUserSender());

		this.validator.validate(result, binding);
		return result;
	}

	public Chirp reconstruct(final Chirp message, final BindingResult binding) {
		Chirp result = new Chirp();

		if (message.getId() == 0)
			result = message;
		else {
			result.setChirpRecipient(message.getChirpRecipient());
			result.setChirpSender(message.getChirpSender());
			result.setText(message.getText());
			result.setSubject(message.getSubject());
			result.setAttachments(message.getAttachments());
			result.setMoment(message.getMoment());

			this.validator.validate(result, binding);
		}

		return result;
	}

	public boolean checkUrls(final String urls) {
		boolean fine = true;
		final String[] urlSplitted = urls.split(",");
		for (final String url : urlSplitted)
			if (!ChirpService.isUrl(url)) {
				fine = false;
				break;
			}
		return fine;
	}

	private static boolean isUrl(final String s) {
		final String regex = "(https?:\\/\\/(?:www\\.)[^\\s\\.]+\\.[^\\s]{2,}|ftp?:\\/\\/[^\\s\\.]+\\.[^\\s]{2,}|http?:\\/\\/[^\\s\\.]+\\.[^\\s]{2,})";

		try {
			final Pattern patt = Pattern.compile(regex);
			final Matcher matcher = patt.matcher(s);
			return matcher.matches();

		} catch (final RuntimeException e) {
			return false;
		}
	}

	public void checkPrincipal(final Chirp m) {
		final Chorbi me = this.chorbiService.findByPrincipal();
		Assert.isTrue((m.getChirpRecipient() != null && m.getChirpRecipient().equals(me)) || (m.getChirpSender() != null && m.getChirpSender().equals(me)));
	}

	public void flush() {
		this.chirpRepository.flush();
	}

	private String mask(final String text) {
		String res = "";
		try {
			final String a = text.replaceFirst("(([+])([0-9]{1,3})([ ])?)?(([0-9]{3}([ ])?[0-9]{3}([ ])?[0-9]{3})|([0-9]{3}([ ])?[0-9]{2}([ ])?[0-9]{2}([ ])?[0-9]{2}))", "***");
			res = a.replaceFirst("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", "***");
		} catch (final Throwable oops) {
			System.out.println(oops.getMessage());
		}
		return res;

	}
}
