
package services;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.ChorbiRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import domain.Actor;
import domain.Chirp;
import domain.Chorbi;
import domain.Coordinates;
import domain.Likes;
import domain.SearchTemplate;
import forms.FormChorbi;

@Transactional
@Service
public class ChorbiService {

	// Managed repository -------------------------------
	@Autowired
	private ChorbiRepository		chorbiRepository;

	@Autowired
	private Validator				validator;

	// Managed services -------------------------------
	@Autowired
	private LoginService			loginService;
	@Autowired
	private SearchTemplateService	searchTemplateService;
	@Autowired
	private ActorService			actorService;


	// Constructor --------------------------------------
	public ChorbiService() {
		super();
	}

	// Simple CRUD methods ------------------------------
	public Chorbi create() {
		final Chorbi result = new Chorbi();
		result.setBanned(false);

		result.setLikesReceived(new HashSet<Likes>());
		result.setLikesSent(new HashSet<Likes>());
		result.setChirpsReceived(new HashSet<Chirp>());
		result.setChirpsSent(new HashSet<Chirp>());
		result.setSearch(null);
		final Coordinates c = new Coordinates();
		result.setCoordinates(c);
		return result;
	}

	public Chorbi save(final Chorbi c) {
		Assert.notNull(c);
		String password;
		String hash;

		password = c.getUserAccount().getPassword();
		hash = this.encodePassword(password);
		c.getUserAccount().setPassword(hash);
		c.setDescription(this.mask(c.getDescription()));
		c.setName(this.mask(c.getName()));
		c.setSurname(this.mask(c.getSurname()));
		c.getCoordinates().setCity(this.mask(c.getCoordinates().getCity()));
		c.getCoordinates().setCountry(this.mask(c.getCoordinates().getCountry()));
		c.getCoordinates().setProvince(this.mask(c.getCoordinates().getProvince()));
		c.getCoordinates().setState(this.mask(c.getCoordinates().getState()));
		Chorbi res = this.chorbiRepository.save(c);

		final SearchTemplate st = this.searchTemplateService.create(res);
		c.setSearch(st);
		final SearchTemplate stSave = this.searchTemplateService.save(st);
		res.setSearch(stSave);

		res = this.chorbiRepository.save(res);
		return res;

	}
	public Chorbi edit(final Chorbi c) {
		Assert.notNull(c);
		c.setDescription(this.mask(c.getDescription()));
		c.setName(this.mask(c.getName()));
		c.setSurname(this.mask(c.getSurname()));
		return this.chorbiRepository.save(c);
	}
	public Chorbi findOne(final Integer id) {
		return this.chorbiRepository.findOne(id);
	}

	public Collection<Chorbi> findAll() {
		return this.chorbiRepository.findAll();
	}

	public Chorbi reconstruct(final FormChorbi formChorbi, final BindingResult binding) {

		final Chorbi result = new Chorbi();
		result.setId(formChorbi.getId());
		result.setVersion(formChorbi.getVersion());
		result.setName(formChorbi.getName());
		result.setSurname(formChorbi.getSurname());
		final UserAccount userAccount = formChorbi.getUserAccount();
		result.setUserAccount(userAccount);
		result.setPhone(formChorbi.getPhone());
		result.setEmail(formChorbi.getEmail());
		result.setGenre(formChorbi.getGenre());
		result.setPicture(formChorbi.getPicture());
		result.setBirthDate(formChorbi.getBirthDate());
		result.setRelationship(formChorbi.getRelationship());
		result.setDescription(formChorbi.getDescription());
		result.setBanned(formChorbi.isBanned());
		result.setSearch(formChorbi.getSearch());
		result.setCoordinates(formChorbi.getCoordinates());
		result.setChirpsReceived(formChorbi.getChirpsReceived());
		result.setChirpsSent(formChorbi.getChirpsSent());
		result.setLikesReceived(formChorbi.getLikesReceived());
		result.setLikesSent(formChorbi.getLikesSent());

		this.validator.validate(result, binding);
		return result;
	}

	public Chorbi reconstruct(final Chorbi chorbi, final BindingResult binding) {
		Chorbi result;

		if (chorbi.getId() == 0)
			result = chorbi;
		else {
			result = this.chorbiRepository.findOne(chorbi.getId());
			result.setName(chorbi.getName());
			result.setSurname(chorbi.getSurname());
			result.setEmail(chorbi.getName());
			result.setPhone(chorbi.getPhone());
			result.setPicture(chorbi.getPicture());
			result.setDescription(chorbi.getDescription());
			result.setBirthDate(chorbi.getBirthDate());
			result.setGenre(chorbi.getGenre());
			result.setCoordinates(chorbi.getCoordinates());
			result.setCreditCard(chorbi.getCreditCard());
			result.setBanned(chorbi.isBanned());

			this.validator.validate(result, binding);
		}

		return result;
	}

	@SuppressWarnings("static-access")
	public Chorbi findByPrincipal() {
		UserAccount userAccount;
		Chorbi result;

		userAccount = this.loginService.getPrincipal();
		result = this.findByUserAccount(userAccount);
		return result;
	}

	public Chorbi findByUserAccount(final UserAccount userAccount) {
		Chorbi result;

		result = this.chorbiRepository.findByUserAccount(userAccount.getId());
		return result;

	}

	public Collection<Object> listNumberChorbiesForCity() {
		return this.chorbiRepository.listNumberChorbiesForCity();
	}

	public Collection<Object> listNumberChorbiesForCountry() {
		return this.chorbiRepository.listNumberChorbiesForCountry();
	}

	public Collection<Object> calculateMinMaxAvgAgesOfChorbies() {
		return this.chorbiRepository.calculateMinMaxAvgAgesOfChorbies();
	}

	public Collection<Object> calculateRatioOfChorbiesWhoHaveNotCreditCard() {
		return this.chorbiRepository.calculateRatioOfChorbiesWhoHaveNotCreditCard();
	}

	public Collection<Object> calculateRatioOfChorbiesWhoSearchActivities() {
		return this.chorbiRepository.calculateRatioOfChorbiesWhoSearchActivities();
	}

	public Collection<Object> calculateRatioOfChorbiesWhoSearchFriendShip() {
		return this.chorbiRepository.calculateRatioOfChorbiesWhoSearchFriendShip();
	}

	public Collection<Object> listChorbiesSortedLikes() {
		return this.chorbiRepository.listChorbiesSortedLikes();
	}

	public Collection<Object> calculateMinMaxAvgLikesOfChorbies() {
		return this.chorbiRepository.calculateMinMaxAvgLikesOfChorbies();
	}

	public Collection<Object> findChorbiesWhoReceivedMoreChips() {
		return this.chorbiRepository.findChorbiesWhoReceivedMoreChips();
	}

	public Collection<Object> findChorbiesWhoSentMoreChips() {
		return this.chorbiRepository.findChorbiesWhoSentMoreChips();
	}

	private String encodePassword(final String password) {
		Md5PasswordEncoder encoder;
		String result;

		if (password == null || "".equals(password))
			result = null;
		else {
			encoder = new Md5PasswordEncoder();
			result = encoder.encodePassword(password, null);
		}

		return result;
	}

	public void flush() {
		this.chorbiRepository.flush();
	}

	public FormChorbi createForm() {
		final FormChorbi formChorbi = new FormChorbi();
		formChorbi.setBanned(false);
		final Authority a = new Authority();
		final UserAccount userAccount = new UserAccount();
		a.setAuthority("CHORBI");
		userAccount.addAuthority(a);
		formChorbi.setUserAccount(userAccount);

		formChorbi.setCoordinates(new Coordinates());
		return formChorbi;
	}

	public Collection<Object> calculateRatioOfChorbiesWhoSearchLove() {

		return this.chorbiRepository.calculateRatioOfChorbiesWhoSearchLove();
	}

	public static int getAge(final Date dateOfBirth) {

		final Calendar today = Calendar.getInstance();
		final Calendar birthDate = Calendar.getInstance();

		int age = 0;

		birthDate.setTime(dateOfBirth);
		if (birthDate.after(today))
			throw new IllegalArgumentException("Can't be born in the future");

		age = today.get(Calendar.YEAR) - birthDate.get(Calendar.YEAR);

		// If birth date is greater than todays date (after 2 days adjustment of leap year) then decrement age one year   
		if ((birthDate.get(Calendar.DAY_OF_YEAR) - today.get(Calendar.DAY_OF_YEAR) > 3) || (birthDate.get(Calendar.MONTH) > today.get(Calendar.MONTH)))
			age--;
		else if ((birthDate.get(Calendar.MONTH) == today.get(Calendar.MONTH)) && (birthDate.get(Calendar.DAY_OF_MONTH) > today.get(Calendar.DAY_OF_MONTH)))
			age--;

		return age;
	}

	public Collection<Chorbi> findChorbiesBySearch(final String city, final String country, final String state, final String province, final String genre, final String keyword, final String relationship) {
		return this.chorbiRepository.findChorbiesBySearch(city, country, state, province, genre, keyword, relationship);
	}
	public Collection<Object> minMaxAvgChirpRevieced() {

		return this.chorbiRepository.minMaxAvgChirpRevieced();
	}

	public Collection<Object> minMaxAvgChirpSent() {

		return this.chorbiRepository.minMaxAvgChirpSent();
	}

	public Chorbi ban(final Chorbi o) {
		Assert.notNull(o);
		this.checkPrincipal();
		o.setBanned(true);
		return this.chorbiRepository.save(o);

	}

	public Chorbi unban(final Chorbi o) {
		Assert.notNull(o);
		this.checkPrincipal();
		o.setBanned(false);
		return this.chorbiRepository.save(o);

	}

	private String mask(final String text) {
		String res = "";
		try {
			final String a = text.replaceFirst("(([+])([0-9]{1,3})([ ])?)?(([0-9]{3}([ ])?[0-9]{3}([ ])?[0-9]{3})|([0-9]{3}([ ])?[0-9]{2}([ ])?[0-9]{2}([ ])?[0-9]{2}))", "***");
			res = a.replaceFirst("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", "***");
		} catch (final Throwable oops) {
			System.out.println(oops.getMessage());
		}
		return res;

	}

	public Chorbi findByUsername(final String username) {
		return this.chorbiRepository.findByUsername(username);
	}

	public void checkPrincipal() {
		final Actor a = this.actorService.findByPrincipal();
		for (final Authority b : a.getUserAccount().getAuthorities())
			Assert.isTrue(b.getAuthority().equals("ADMINISTRATOR"));

	}

}
