
package services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.ConfigurationRepository;
import domain.Configuration;

@Service
@Transactional
public class ConfigurationService {

	// Managed repository -------------------------------
	@Autowired
	private ConfigurationRepository	configurationRepository;

	@Autowired
	private Validator				validator;


	// Constructor --------------------------------------
	public ConfigurationService() {
		super();
	}

	// Simple CRUD methods ------------------------------

	public Configuration create() {
		final Configuration f = new Configuration();
		f.setBanners("");
		f.setSearchHours(":::");
		return f;
	}
	public Configuration save(final Configuration f) {
		Assert.notNull(f);

		return this.configurationRepository.save(f);
	}

	public Configuration findAll() {

		return this.configurationRepository.findAll().get(0);
	}

	// Other business methods ---------------------------

	public Configuration reconstruct(final Configuration configuration, final BindingResult binding) {
		Configuration result;

		if (configuration.getId() == 0)
			result = configuration;
		else {
			result = this.configurationRepository.findOne(configuration.getId());

			result.setBanners(configuration.getBanners());
			result.setSearchHours(configuration.getSearchHours());
			this.validator.validate(result, binding);
		}

		return result;
	}

	public void flush() {
		this.configurationRepository.flush();
	}

}
