
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.LikesRepository;
import domain.Chorbi;
import domain.Likes;
import forms.FormLikes;

@Service
@Transactional
public class LikesService {

	// Managed repository -------------------------------
	@Autowired
	private LikesRepository	likesRepository;

	@Autowired
	private ChorbiService	chorbiService;

	@Autowired
	private Validator		validator;


	// Constructor --------------------------------------
	public LikesService() {
		super();
	}

	// Simple CRUD methods ------------------------------
	public Likes create(final Chorbi c) {

		final Likes m = new Likes();
		final Chorbi likeSender = this.chorbiService.findByPrincipal();

		final Date currentDate = new Date();
		currentDate.setTime(currentDate.getTime() - 600000);
		m.setMoment(currentDate);
		m.setComment("");

		m.setLikesSender(likeSender);
		m.setLikesRecipient(c);

		return m;
	}

	public Likes save(final Likes mm) {
		Assert.notNull(mm);
		mm.setComment(this.mask(mm.getComment()));
		return this.likesRepository.save(mm);
	}

	public void deleteWR(final Likes m) {
		this.likesRepository.delete(m.getId());
	}

	public void delete(final Likes m, final boolean sender) {
		this.checkPrincipal(m);
		final Chorbi connected = this.chorbiService.findByPrincipal();
		if (m.getLikesRecipient() == connected && !sender) {
			final Collection<Likes> cm = connected.getLikesReceived();
			cm.remove(m);
			connected.setLikesReceived(cm);
			this.chorbiService.edit(connected);
			m.setLikesRecipient(null);
		} else if (m.getLikesSender() == connected && sender) {
			final Collection<Likes> cm = connected.getLikesSent();
			cm.remove(m);
			connected.setLikesSent(cm);
			this.chorbiService.edit(connected);
			m.setLikesSender(null);
		}
		if (m.getLikesRecipient() == null && m.getLikesSender() == null)
			this.likesRepository.delete(m);

	}

	// Other Likes methods ---------------------------

	public Collection<Likes> findByPrincipal() {
		Collection<Likes> result = new ArrayList<Likes>();
		final Chorbi chorbi = this.chorbiService.findByPrincipal();

		result = this.likesRepository.findByChorbi(chorbi);
		return result;
	}

	public Likes findOne(final int id) {
		return this.likesRepository.findOne(id);
	}

	public Likes reconstruct(final FormLikes formLikes, final BindingResult binding) {
		final Likes result = new Likes();

		result.setMoment(formLikes.getMoment());
		result.setComment(formLikes.getComment());

		this.validator.validate(result, binding);
		return result;
	}

	public Likes reconstruct(final Likes message, final BindingResult binding) {
		Likes result = new Likes();

		if (message.getId() == 0)
			result = message;
		else {
			result.setLikesRecipient(message.getLikesRecipient());
			result.setLikesSender(message.getLikesSender());

			result.setMoment(message.getMoment());
			result.setComment(message.getComment());

			this.validator.validate(result, binding);
		}

		return result;
	}
	public void checkPrincipal(final Likes m) {
		final Chorbi me = this.chorbiService.findByPrincipal();
		Assert.isTrue((m.getLikesRecipient() != null && m.getLikesRecipient().equals(me)) || (m.getLikesSender() != null && m.getLikesSender().equals(me)));
	}

	public void flush() {
		this.likesRepository.flush();
	}

	public void like(final Chorbi c, final Likes l) {
		final Chorbi sender = this.chorbiService.findByPrincipal();
		final Chorbi recipient = c;

		final Collection<Likes> clr = recipient.getLikesReceived();
		final Collection<Likes> cls = sender.getLikesSent();
		clr.add(l);
		cls.add(l);
		recipient.setLikesReceived(clr);
		sender.setLikesSent(cls);
		this.chorbiService.edit(sender);
		this.chorbiService.edit(recipient);

	}

	public void dislike(final Likes l) {
		final Chorbi sender = this.chorbiService.findByPrincipal();
		final Chorbi recipient = l.getLikesRecipient();

		final Collection<Likes> clr = recipient.getLikesReceived();
		final Collection<Likes> cls = sender.getLikesSent();
		clr.remove(l);
		cls.remove(l);
		recipient.setLikesReceived(clr);
		sender.setLikesSent(cls);
		this.chorbiService.edit(sender);
		this.chorbiService.edit(recipient);

		this.likesRepository.delete(l);

	}

	private String mask(final String text) {
		String res = "";
		try {
			final String a = text.replaceFirst("(([+])([0-9]{1,3})([ ])?)?([0-9]{3}([ ])?[0-9]{3}([ ])?[0-9]{3})", "***");
			res = a.replaceFirst("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", "***");
		} catch (final Throwable oops) {
			System.out.println(oops.getMessage());
		}
		return res;

	}
}
