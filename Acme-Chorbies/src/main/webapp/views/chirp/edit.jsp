<%--
 * edit.jsp
 *
 * Copyright (C) 2016 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<form:form action="chirp/chorbi/send.do" modelAttribute="formChirp"
	method="POST">
	<jstl:if test="${create != null}">
		<form:hidden path="moment" />
		<form:hidden path="userSender" />
	</jstl:if>
	<table id="formus">
		<jstl:if test="${create == null}">
			<tr>
				<td><form:label path="userSender">
						<spring:message code="chirp.sender" />: </form:label></td>
				<td><jstl:out value="${formChirp.userSender}" /></td>
			</tr>
		</jstl:if>


		<jstl:if test="${create == null}">
			<tr>
				<td><form:label path="userRecipient">
						<spring:message code="chirp.recipient" />: </form:label></td>
				<td><jstl:out value="${formChirp.userRecipient}" /></td>
			</tr>
		</jstl:if>
		<jstl:if test="${create==true}">
			<acme:textbox code="chirp.recipient" path="userRecipient" />
		</jstl:if>

		<tr>
			<td><jstl:if test="${!create}">
					<form:label path="subject">
						<spring:message code="chirp.subject" />:</td>
			<td></form:label> <jstl:out value="${formChirp.subject}" /><br /> </jstl:if> <jstl:if test="${create}">
					<acme:textbox code="chirp.subject" path="subject" />
				</jstl:if></td>
		<tr>
			<td><jstl:if test="${!create}">
					<form:label path="text">
						<spring:message code="chirp.text" />: </td>
			<td></form:label> <jstl:out value="${formChirp.text}" /><br /> </jstl:if> <jstl:if test="${create}">
					<acme:textarea code="chirp.text" path="text" />
				</jstl:if></td>
		</tr>
		<tr>
			<td><jstl:if test="${!create}">
					<tr>
						<td><form:label path="attachments">
								<spring:message code="chirp.attachments" />:
		</form:label></td>

						<td><jstl:forTokens items="${formChirp.attachments}"
								delims="," var="mySplit">
								<a href='<jstl:out value="${mySplit}"/>'><jstl:out
										value="${mySplit}" /></a>
								<br />
							</jstl:forTokens></td>
					</tr>
				</jstl:if> <jstl:if test="${create}">
					<acme:textbox code="chirp.attachments" path="attachments" />

				</jstl:if>
	</table>

	<jstl:if test="${create}">
		<acme:submit name="save" code="chirp.send" />&nbsp; 
	</jstl:if>
	<acme:cancel url='${ruta}' code="chirp.cancel" />

	<jstl:if test="${create}">
		<br />
		<br />
					
						( <spring:message code="chirp.note" /> )
						
					
				</jstl:if>
</form:form>