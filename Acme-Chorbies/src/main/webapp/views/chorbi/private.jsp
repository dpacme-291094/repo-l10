<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<form:form action="chorbi/private.do" modelAttribute="chorbi">

	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="userAccount.Authorities" />
	<form:hidden path="userAccount" />
	<form:hidden path="likesReceived" />
	<form:hidden path="likesSent" />
	<form:hidden path="chirpsReceived" />
	<form:hidden path="chirpsSent" />
	<form:hidden path="search" />
	<form:hidden path="banned" />

	<form:hidden path="creditCard.holderName" />
	<form:hidden path="creditCard.brandName" />
	<form:hidden path="creditCard.number" />
	<form:hidden path="creditCard.expirationMonth" />
	<form:hidden path="creditCard.expirationYear" />
	<form:hidden path="creditCard.codeCVV" />

	<form:label path="picture">

		<img src="${chorbi.picture}" height="200" width="150" />
	</form:label>
	<table id="formus">
		<tr>
			<td><u>
					<h3>
						<spring:message code="chorbi.personal" />
					</h3>
			</u></td>
		</tr>
		<acme:textbox code="chorbi.name" path="name" />
		<acme:textbox code="chorbi.surname" path="surname" />
		<acme:textbox code="chorbi.email" path="email" />
		<acme:textbox code="chorbi.phone" path="phone" />
		<acme:textbox code="chorbi.picture" path="picture" />
		<acme:textarea code="chorbi.description" path="description" />

		<acme:selectModified code="chorbi.relationship" path="relationship"
			items="${relationships}" id="relationship" />

		<acme:textbox code="chorbi.birthDate" path="birthDate" />


		<acme:selectModified code="chorbi.genre" path="genre"
			items="${genres}" id="genre" />
		<tr>
			<td><u>
					<h3>
						<spring:message code="chorbi.coordinates" />
					</h3>
			</u></td>
		</tr>
		<acme:textbox code="chorbi.city" path="coordinates.city" />
		<acme:textbox code="chorbi.country" path="coordinates.country" />
		<acme:textbox code="chorbi.state" path="coordinates.state" />
		<acme:textbox code="chorbi.province" path="coordinates.province" />
		<tr>
			<td></td>
		</tr>
		<tr>
			<td><a href="chorbies/chorbi/likesList.do?chorbiId=${chorbi.id}"><spring:message
						code="chorbi.likes" /></a></td>
		</tr>
	</table>
	<acme:submit name="save" code="chorbi.save" />

	<acme:cancel url="welcome/index.do" code="chorbi.cancel" />


</form:form>
<form:form method="POST" action="chorbi/creditCard.do">
	<table id="formus">
		<tr>
			<td><u>
					<h3>
						<spring:message code="chorbi.cc" />
					</h3>
			</u></td>
		</tr>

		<jstl:if test="${empty chorbi.creditCard.holderName }">
			<tr>
				<td><spring:message code="cc.null" /></td>
			</tr>
		</jstl:if>
		<jstl:if test="${not empty chorbi.creditCard.holderName}">
			<tr>
				<td><b><spring:message code="chorbi.creditCardHolderName" />
						: </b> <jstl:out value="${chorbi.creditCard.holderName}" /></td>
			</tr>
			<tr>
				<td><b><spring:message code="chorbi.creditCardBrandName" />
						: </b> <jstl:out value="${chorbi.creditCard.brandName}" /></td>
			</tr>
			<tr>
				<td><b><spring:message
							code="chorbi.creditCardExpirationYear" /> : </b> <jstl:out
						value="${chorbi.creditCard.expirationYear}" /></td>
			</tr>
			<tr>
				<td><b><spring:message
							code="chorbi.creditCardExpirationMonth" /> : </b> <jstl:out
						value="${chorbi.creditCard.expirationMonth}" /></td>
			</tr>
			<tr>
				<td><b><spring:message code="chorbi.creditCardNumber" /> :
				</b> <jstl:out value="${cc}" /></td>
			</tr>
			<tr>
				<td><b><spring:message code="chorbi.creditCardCVV" /> : </b> <jstl:out
						value="${chorbi.creditCard.codeCVV}" /></td>
			</tr>
		</jstl:if>
	</table>
	<td><input type="submit" name="edit2"
		value="<spring:message code="chorbi.newcc" />" />&nbsp;
</form:form>


