<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>
<table id="formus">
	<tr>
		<td><u>
				<h3>
					<spring:message code="chorbi.personal" />
				</h3>
		</u></td>
	</tr>

	<tr>
		<td><img src="${chorbi.picture}" height="200" width="150"></td>
	</tr>
	<tr>
		<td></td>
	</tr>
	<tr>
		<td><b><spring:message code="chorbi.userAccount" /></b> <br />
			<jstl:out value="${chorbi.userAccount.username}" /></td>
	</tr>
	<tr>
		<td><b><spring:message code="chorbi.name" /></b> <br /> <jstl:out
				value="${chorbi.name}" /></td>
	</tr>
	<tr>
		<td><b><spring:message code="chorbi.surname" /></b> <br /> <jstl:out
				value="${chorbi.surname}" /></td>
	</tr>
	<tr>
		<td><b><spring:message code="chorbi.description" /></b> <br />
			<jstl:out value="${chorbi.description}" /></td>
	</tr>
	<tr>
		<td><b><spring:message code="chorbi.looking" /></b> <br /> <jstl:out
				value="${chorbi.relationship}" /></td>
	</tr>
	<tr>
		<td><b><spring:message code="chorbi.age" /></b> <br /> <jstl:out
				value="${age}" /></td>
	</tr>
	<tr>
		<td><b><spring:message code="chorbi.genre" /></b> <br /> <jstl:if
				test="${chorbi.genre=='man'}">
				<spring:message code="chorbi.man" />


			</jstl:if> <jstl:if test="${chorbi.genre=='woman'}">
				<spring:message code="chorbi.woman" />


			</jstl:if></td>
	</tr>
	<tr>
		<td><u>
				<h3>
					<spring:message code="chorbi.coordinates" />
				</h3>
		</u></td>
	</tr>

	<jstl:if test="${not empty chorbi.coordinates.city }">
		<tr>
			<td><b><spring:message code="chorbi.city" /></b> <br /> <jstl:out
					value="${chorbi.coordinates.city}" /></td>
		</tr>
	</jstl:if>
	<jstl:if test="${not empty chorbi.coordinates.country }">
		<tr>
			<td><b><spring:message code="chorbi.country" /></b> <br /> <jstl:out
					value="${chorbi.coordinates.country}" /></td>
		</tr>
	</jstl:if>
	<jstl:if test="${not empty chorbi.coordinates.state }">
		<tr>
			<td><b><spring:message code="chorbi.state" /></b> <br /> <jstl:out
					value="${chorbi.coordinates.state}" /></td>
		</tr>
	</jstl:if>
	<jstl:if test="${not empty chorbi.coordinates.province }">
		<tr>
			<td><b><spring:message code="chorbi.province" /></b> <br /> <jstl:out
					value="${chorbi.coordinates.province}" /></td>
		</tr>
	</jstl:if>
	<tr>
		<td></td>
	</tr>

	<tr>
		<td><a href="chorbies/chorbi/likesList.do?chorbiId=${chorbi.id}"><spring:message
					code="chorbi.likes" /></a></td>
	</tr>

</table>
<jstl:if test="${myself==false}">
	<jstl:if test="${following==false}">
		<form:form method="POST" action="chorbies/chorbi/profile.do">
			<input type="hidden" name="chorbiId" value="${chorbi.id}" />

			<input type="submit" name="like"
				value="<spring:message code="chorbi.like" />" />
		</form:form>
	</jstl:if>
	<jstl:if test="${following==true}">
		<form:form method="POST" action="chorbies/chorbi/profile.do">
			<input type="hidden" name="likeId" value="${likeId}" />

			<input type="submit" name="dislike"
				value="<spring:message code="chorbi.dislike" />" />
		</form:form>
	</jstl:if>
	<br />
	<form:form method="POST" action="chirp/chorbi/create.do">
		<input type="hidden" name="id" value="${chorbi.id}" />

		<input type="submit" name="create"
			value="<spring:message code="chirp.create" />" />
	</form:form>
</jstl:if>