<%--
 * list.jsp
 *
 * Copyright (C) 2016 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!-- C Queries -->

<h2>
	<spring:message code="report.cfc" />
	:
</h2>

<table>
	<jstl:forEach items="${cfc}" var="rori">
		<tr>
			<td><jstl:out value="${rori[0]}" /></td>
			<td><fmt:formatNumber type="number" maxFractionDigits="2"
					value="${rori[1]}" /></td>
		</tr>
	</jstl:forEach>
</table>
<table>
	<jstl:forEach items="${cfc2}" var="rori">
		<tr>
			<td><jstl:out value="${rori[0]}" /></td>
			<td><fmt:formatNumber type="number" maxFractionDigits="2"
					value="${rori[1]}" /></td>
		</tr>
	</jstl:forEach>
</table>

<h2>
	<spring:message code="report.aoc" />
	:
</h2>

<table>
	<tr>
		<th><spring:message code="report.min" />:</th>
		<th><spring:message code="report.avg" />:</th>
		<th><spring:message code="report.max" />:</th>
	</tr>

	<jstl:forEach items="${aoc}" var="consultaA1">
		<tr>

			<td><fmt:formatNumber type="number" maxFractionDigits="0"
					value="${consultaA1[0]}" /></td>
			<td><fmt:formatNumber type="number" maxFractionDigits="0"
					value="${consultaA1[2]}" /></td>
			<td><fmt:formatNumber type="number" maxFractionDigits="0"
					value="${consultaA1[1]}" /></td>
		</tr>
	</jstl:forEach>

</table>
<h2>
	<spring:message code="report.nvcc" />
	:
</h2>

<table>
	<tr>

		<td><fmt:formatNumber type="number" maxFractionDigits="2"
				value="${nvcc[0]}" /></td>
	</tr>
</table>

<h2>
	<spring:message code="report.rca" />
	:
</h2>
<table>
	<tr>
		<th><spring:message code="report.activities" />:</th>
		<th><spring:message code="report.love" />:</th>
		<th><spring:message code="report.friendship" />:</th>
	</tr>

	<tr>

		<td><fmt:formatNumber type="number" maxFractionDigits="2"
				value="${rca[0]}" /></td>
		<td><fmt:formatNumber type="number" maxFractionDigits="2"
				value="${rcl[0]}" /></td>
		<td><fmt:formatNumber type="number" maxFractionDigits="2"
				value="${rcf[0]}" /></td>
	</tr>
</table>

<h2>
	<spring:message code="report.csl" />
	:
</h2>

<table>
	<jstl:forEach items="${csl}" var="rori">
		<tr>
			<td><jstl:out value="${rori[0].name}" /></td>
			<td><jstl:out value="${rori[1]}" /></td>
		</tr>
	</jstl:forEach>
</table>

<h2>
	<spring:message code="report.mal" />
	:
</h2>

<table>
	<tr>
		<th><spring:message code="report.min" />:</th>
		<th><spring:message code="report.avg" />:</th>
		<th><spring:message code="report.max" />:</th>
	</tr>

	<jstl:forEach items="${mal}" var="consultaA1">
		<tr>

			<td><fmt:formatNumber type="number" maxFractionDigits="0"
					value="${consultaA1[0]}" /></td>
			<td><fmt:formatNumber type="number" maxFractionDigits="0"
					value="${consultaA1[2]}" /></td>
			<td><fmt:formatNumber type="number" maxFractionDigits="0"
					value="${consultaA1[1]}" /></td>
		</tr>
	</jstl:forEach>

</table>

<h2>
	<spring:message code="report.mavcr" />
	:
</h2>

<table>
	<tr>
		<th><spring:message code="report.min" />:</th>
		<th><spring:message code="report.avg" />:</th>
		<th><spring:message code="report.max" />:</th>
	</tr>

	<jstl:forEach items="${mavcr}" var="consultaA1">
		<tr>

			<td><fmt:formatNumber type="number" maxFractionDigits="0"
					value="${consultaA1[0]}" /></td>
			<td><fmt:formatNumber type="number" maxFractionDigits="0"
					value="${consultaA1[2]}" /></td>
			<td><fmt:formatNumber type="number" maxFractionDigits="0"
					value="${consultaA1[1]}" /></td>
		</tr>
	</jstl:forEach>

</table>

<h2>
	<spring:message code="report.mavcs" />
	:
</h2>

<table>
	<tr>
		<th><spring:message code="report.min" />:</th>
		<th><spring:message code="report.avg" />:</th>
		<th><spring:message code="report.max" />:</th>
	</tr>

	<jstl:forEach items="${mavcs}" var="consultaA1">
		<tr>

			<td><fmt:formatNumber type="number" maxFractionDigits="0"
					value="${consultaA1[0]}" /></td>
			<td><fmt:formatNumber type="number" maxFractionDigits="0"
					value="${consultaA1[2]}" /></td>
			<td><fmt:formatNumber type="number" maxFractionDigits="0"
					value="${consultaA1[1]}" /></td>
		</tr>
	</jstl:forEach>

</table>

<h2>
	<spring:message code="report.crmc" />
	:
</h2>

<table>
	<jstl:forEach items="${crmc}" var="rori">
		<tr>
			<td><jstl:out value="${rori[0].name}" /></td>
			<td><jstl:out value="${rori[1]}" /></td>
		</tr>
	</jstl:forEach>
</table>

<h2>
	<spring:message code="report.csmc" />
	:
</h2>

<table>
	<jstl:forEach items="${csmc}" var="rori">
		<tr>
			<td><jstl:out value="${rori[0].name}" /></td>
			<td><jstl:out value="${rori[1]}" /></td>
		</tr>
	</jstl:forEach>
</table>

