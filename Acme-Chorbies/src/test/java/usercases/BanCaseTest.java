
package usercases;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import services.ChorbiService;
import utilities.AbstractTest;
import domain.Chorbi;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@Transactional
public class BanCaseTest extends AbstractTest {

	@Autowired
	private ChorbiService	chorbiService;


	/*
	 * Requisito funcional: Ban a chorbi, that is, to disable his or her account.
	 * Se comprueba en el siguiente orden:
	 * El administrador banea a un chorbi que existe
	 * Se intenta banear a un chorbi existente sin loguearse
	 * El admin intenta banear a un chorbi inexistente
	 * Un usuario logueado como chorbi intenta banear a un chorbi existente
	 */
	@Test
	public void driverBanChorbiData() {

		@SuppressWarnings("deprecation")
		final Object testingData[][] = {
			{
				"admin", 26, null
			}, {
				"", 26, IllegalArgumentException.class
			}, {
				"admin", 8999, IllegalArgumentException.class
			}, {
				"chorbi1", 26, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.templateUnban((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}

	/*
	 * Requisito funcional: Unban a chorbi, which means that his or her account is re-enabled.
	 * Se comprueba en el siguiente orden:
	 * El administrador desbanea a un chorbi que existe
	 * Se intenta desbanear a un chorbi existente sin loguearse
	 * El admin intenta desbanear a un chorbi inexistente
	 * Un usuario logueado como chorbi intenta desbanear a un chorbi existente
	 */
	@Test
	public void driverUnbanChorbiData() {

		@SuppressWarnings("deprecation")
		final Object testingData[][] = {
			{
				"admin", 26, null
			}, {
				"", 26, IllegalArgumentException.class
			}, {
				"admin", 8999, IllegalArgumentException.class
			}, {
				"chorbi1", 26, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.templateUnban((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}

	protected void templateBan(final String username, final Integer chorbiId, final Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			this.authenticate(username);
			final Chorbi chorbi = this.chorbiService.findOne(chorbiId);
			this.chorbiService.ban(chorbi);
			this.chorbiService.flush();
			this.unauthenticate();

		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

	protected void templateUnban(final String username, final Integer chorbiId, final Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			this.authenticate(username);
			final Chorbi chorbi = this.chorbiService.findOne(chorbiId);
			this.chorbiService.unban(chorbi);
			this.chorbiService.flush();
			this.unauthenticate();

		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

}
