
package usercases;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import services.ChirpService;
import services.ChorbiService;
import utilities.AbstractTest;
import domain.Chirp;
import domain.Chorbi;

/**
 * @author Student
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@Transactional
public class ChirpCaseTest extends AbstractTest {

	@Autowired
	private ChirpService	chirpService;
	@Autowired
	private ChorbiService	chorbiService;


	//Requisito Funcional: Chirp to another chorbi (incluye re-envío y responder)
	/*
	 * En el siguiente orden comprueba:
	 * Envio de un chirp con todos los datos correctos.(correcto)
	 * Envio de un chirp con un chorbi erroneo. (erroneo)
	 * Envio de un chirp a un username que no existe.(erroneo)
	 * Envio de un chirp con "attachments" con URL mal.(erroneo)
	 * Envio de un chirp con todos los campos en blanco.(erroneo)
	 * Envio de un chirp con HTML en algún campo.(erroneo)
	 * Envio de un chirp sin estar autenticado.(erroneo)
	 */

	@Test
	public void driverSave() {

		final Object testingData[][] = {

			{
				"chorbi1", "chorbi2", "Prueba", "prueba", "", null
			}, {
				"chorbi", "chorbi2", "Prueba", "prueba", "", IllegalArgumentException.class
			}, {
				"chorbi1", "chorb", "Prueba", "prueba", "", IllegalArgumentException.class

			}, {
				"chorbi1", "chorbi2", "Prueba", "prueba", "urlmal", IllegalArgumentException.class

			}, {
				"chorbi1", "", "", "", "", IllegalArgumentException.class
			}, {
				"chorbi1", "chorbi2", "<p>", "<b>", "", ConstraintViolationException.class

			}, {
				null, "chorbi2", "Prueba", "prueba", "", IllegalArgumentException.class
			}

		};

		for (int i = 0; i < testingData.length; i++)
			this.templateEdit((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (Class<?>) testingData[i][5]);

	}

	//Requisito Funcional: Erase any of the chirps that he or she’s got or sent, which requires previous confir-mation.
	/*
	 * Se comprueba en el siguiente orden:
	 * Eliminar correctamente un chirp. (correcto)
	 * Eliminar un chirp sin estar autenticado.(erroneo)
	 * Eliminar un chirp que no es tuyo.(erroneo)
	 * Eliminar un chirp que no existe.(erroneo)
	 */
	@Test
	public void driverDelete() {

		final Object testingData[][] = {

			{
				"chorbi2", 35, null
			}, {
				null, 35, NullPointerException.class
			}, {
				"chorbi1", 35, NullPointerException.class
			}, {
				"chorbi1", 50, NullPointerException.class
			}

		};

		for (int i = 0; i < testingData.length; i++)
			this.templateDelete((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);

	}

	//Requisito Funcional: List the chirps that he/she is got or he/she has sent
	/*
	 * Se comprueba la lista en el siguiente orden:
	 * Acceder a la lista de chirps de un usuario logueado (correcto)
	 * Acceder a la lista de chirps de un usuario no autenticado (erroneo)
	 */
	@Test
	public void driverList() {

		final Object testingData[][] = {

			{
				"chorbi1", null
			}, {
				null, IllegalArgumentException.class
			}

		};

		for (int i = 0; i < testingData.length; i++)
			this.templateList((String) testingData[i][0], (Class<?>) testingData[i][1]);

	}

	protected void templateEdit(final String username, final String recipient, final String subject, final String text, final String attachments, final Class<?> expected) {

		Class<?> caught;

		caught = null;

		try {
			this.authenticate(username);
			final Chirp a = this.chirpService.create();
			a.setUserSender(username);
			a.setUserRecipient(recipient);
			a.setAttachments(attachments);
			a.setSubject(subject);
			a.setText(text);
			this.chirpService.save(a);
			this.chirpService.flush();
			this.unauthenticate();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

	protected void templateList(final String username, final Class<?> expected) {

		Class<?> caught;

		caught = null;

		try {
			this.authenticate(username);
			final Chorbi c = this.chorbiService.findByPrincipal();

			c.getChirpsReceived();
			c.getChirpsSent();
			this.unauthenticate();

		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}

	protected void templateDelete(final String username, final Integer chirpId, final Class<?> expected) {

		Class<?> caught;

		caught = null;

		try {
			this.authenticate(username);
			final Chirp a = this.chirpService.findOne(chirpId);
			this.chirpService.deleteWR(a);
			this.chirpService.flush();
			this.unauthenticate();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}
}
