
package usercases;

import java.util.Collection;
import java.util.Date;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import security.Authority;
import security.UserAccount;
import services.ChorbiService;
import services.SearchTemplateService;
import utilities.AbstractTest;
import domain.Chorbi;
import domain.Coordinates;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@Transactional
public class ChorbiCaseTest extends AbstractTest {

	@Autowired
	private ChorbiService			chorbiService;
	@Autowired
	private SearchTemplateService	searchTemplateService;


	/*
	 * Requisito funcional: Registro como un/a chorbi
	 * Se comprueba en el siguiente orden:
	 * Registrarse con todos los datos correctos (Correcto)
	 * Registrarse con un username que ya existe (Error)
	 * Registrarse sin seguir el patron de telefono ni email (Error)
	 * Registrarse sin coincidir las contraseņas (Erroneo)
	 */

	@Test
	public void driverRegistrarChorbi() {

		@SuppressWarnings("deprecation")
		final Object testingData[][] = {
			{
				"chorbi10", "123456", "123456", "Juana", "Martinez", "jmartines@hotmail.com", "954407447", "https://pbs.twimg.com/profile_images/2120083689/images__1__400x400.jpg", "esta es una descripcion", "love", new Date("10/12/1998 15:20"), "woman",
				false, "Spain", "Andalucia", "Sevilla", "Sevilla", null
			},
			{
				"chorbi1", "123456", "123456", "Juana", "Martinez", "jmartines@hotmail.com", "954407447", "https://pbs.twimg.com/profile_images/2120083689/images__1__400x400.jpg", "esta es una descripcion", "love", new Date("10/12/1998 15:20"), "woman",
				false, "Spain", "Andalucia", "Sevilla", "Sevilla", DataIntegrityViolationException.class
			},
			{
				"chorbi10", "123456", "123456", "Juana", "Martinez", "jmartines@hotmail.com", "123", "urlmal", "esta es una descripcion", "love", new Date("10/12/1998 15:20"), "woman", false, "Spain", "Andalucia", "Sevilla", "Sevilla",
				DataIntegrityViolationException.class
			},
			{
				"chorbi10", "123456", "mal", "Juana", "Martinez", "jmartines@hotmail.com", "123", "urlmal", "esta es una descripcion", "love", new Date("10/12/1998 15:20"), "woman", false, "Spain", "Andalucia", "Sevilla", "Sevilla",
				IllegalArgumentException.class
			}

		};

		for (int i = 0; i < testingData.length; i++)
			this.templateRegister((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5], (String) testingData[i][6],
				(String) testingData[i][7], (String) testingData[i][8], (String) testingData[i][9], (Date) testingData[i][10], (String) testingData[i][11], (Boolean) testingData[i][12], (String) testingData[i][13], (String) testingData[i][14],
				(String) testingData[i][15], (String) testingData[i][16], (Class<?>) testingData[i][17]);
	}

	/*
	 * Requisito funcional: Change his or fer profile
	 * Se comprueba en el siguiente orden:
	 * El Chorbi "chorbi1" edita sus datos introduciendolos correctos (correcto)
	 * "null" intenta modificar sus datos (erroneo)
	 * El Chorbi "chorbi1" intenta introducir URL en picture erronea (erroneo)
	 */
	@Test
	public void driverEditPersonalData() {

		@SuppressWarnings("deprecation")
		final Object testingData[][] = {
			{
				"chorbi1", "Juana", "Martinez", "jmartines@hotmail.com", "954407447", "https://pbs.twimg.com/profile_images/2120083689/images__1__400x400.jpg", "esta es una descripcion", "love", new Date("10/12/1998 15:20"), "woman", "Spain", null,
				"Sevilla", "Sevilla", null
			},
			{
				"null", "Juana", "Martinez", "jmartines@hotmail.com", "954407447", "https://pbs.twimg.com/profile_images/2120083689/images__1__400x400.jpg", "esta es una descripcion", "love", new Date("10/12/1998 15:20"), "woman", "Spain", null,
				"Sevilla", "Sevilla", IllegalArgumentException.class
			}, {
				"chorbi1", "Juana", "Martinez", "jmartines@hotmail.com", "954407447", "urlmal", "esta es una descripcion", "love", new Date("10/12/1998 15:20"), "woman", "Spain", null, "Sevilla", "Sevilla", ConstraintViolationException.class
			}

		};

		for (int i = 0; i < testingData.length; i++)
			this.templateEdit((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5], (String) testingData[i][6], (String) testingData[i][7],
				(Date) testingData[i][8], (String) testingData[i][9], (String) testingData[i][10], (String) testingData[i][11], (String) testingData[i][12], (String) testingData[i][13], (Class<?>) testingData[i][14]);
	}

	/*
	 * Requisito funcional: Browse the list of chorbies who have registered to the system
	 * and navigate to the chorbies who likes them.
	 * Se comprueba en el siguiente orden:
	 * Listar la lista registrado como chorbi(Correcto)
	 * Listar la lista registrado como admin(Correcto)
	 * Listar la lista sin estar registrado(Fallo)
	 */
	@Test
	public void driverListChorbies() {

		final Object testingData[][] = {
			{
				"chorbi1", null
			}, {
				"admin", null
			}, {
				null, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.templateList((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}
	protected void templateList(final String username, final Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			this.authenticate(username);
			final Chorbi c = this.chorbiService.findByPrincipal();
			final Collection<Chorbi> cc = this.chorbiService.findAll();
			cc.remove(c);

			for (final Chorbi a : cc)
				a.getLikesReceived();
			this.unauthenticate();

		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

	protected void templateRegister(final String username, final String password, final String repeatPassword, final String name, final String surname, final String email, final String phone, final String picture, final String description,
		final String relationship, final Date birthDate, final String genre, final boolean banned, final String country, final String state, final String province, final String city, final Class<?> expected) {

		Class<?> caught;

		caught = null;

		try {
			final Chorbi c = this.chorbiService.create();

			final UserAccount ua = new UserAccount();
			ua.setUsername(username);
			ua.setPassword(password);
			final Authority aut = new Authority();
			aut.setAuthority("CHORBI");
			ua.addAuthority(aut);
			c.setUserAccount(ua);
			Assert.isTrue(password == repeatPassword);
			c.setEmail(email);
			c.setName(name);
			c.setSurname(surname);
			c.setPhone(phone);

			c.setPicture(picture);
			c.setDescription(description);
			c.setRelationship(relationship);
			c.setBirthDate(birthDate);
			c.setGenre(genre);

			final Coordinates co = new Coordinates();
			co.setCity(city);
			co.setCountry(country);
			co.setProvince(province);
			co.setState(state);

			c.setCoordinates(co);
			c.setCreditCard(null);
			c.setBanned(banned);

			this.chorbiService.save(c);
			this.chorbiService.flush();

		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}

	protected void templateEdit(final String username, final String name, final String surname, final String email, final String phone, final String picture, final String description, final String relationship, final Date birthDate, final String genre,
		final String country, final String state, final String province, final String city, final Class<?> expected) {

		Class<?> caught;

		caught = null;

		try {

			this.authenticate(username);
			final Chorbi c = this.chorbiService.findByPrincipal();

			c.setEmail(email);
			c.setName(name);
			c.setSurname(surname);
			c.setPhone(phone);

			c.setPicture(picture);
			c.setDescription(description);
			c.setRelationship(relationship);
			c.setBirthDate(birthDate);
			c.setGenre(genre);

			c.getCoordinates().setCity(city);
			c.getCoordinates().setCountry(country);
			c.getCoordinates().setProvince(province);
			c.getCoordinates().setState(state);

			this.chorbiService.edit(c);
			this.chorbiService.flush();

			this.unauthenticate();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}

}
