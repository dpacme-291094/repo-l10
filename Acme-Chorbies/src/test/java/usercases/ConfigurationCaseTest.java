
package usercases;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import services.ConfigurationService;
import utilities.AbstractTest;
import domain.Configuration;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@Transactional
public class ConfigurationCaseTest extends AbstractTest {

	// Managed Services ----------------------
	@Autowired
	private ConfigurationService	configurationService;


	// Test -----------------------------

	/*
	 * Requisito funcional: Change the banners that are displayed on the welcome page and the time that the
	 * results of search templates are cached. The time must be expressed in hours,
	 * minutes, and seconds.
	 * Se comprueba en el siguiente orden:
	 * El administrador se loguea correctamente y cambia los banners estableciendo el nuevo tiempo.
	 * Se intenta cambiar los banners sin loguearse
	 */
	@Test
	public void driverChangeBan() {
		@SuppressWarnings("deprecation")
		final Object testingData[][] = {
			{
				"admin", "https://i.ytimg.com/vi/hMKOZfftJ-0/maxresdefault.jpg,http://netvantagemarketing.com/blog/wp-content/uploads/2016/02/bed-and-breakfast-3.jpg,http://www.hirecarandgo.com/wp-content/uploads/2016/03/CarAndGoLogo3.png", "02:00:00",
				null
			},
			{
				"", "https://i.ytimg.com/vi/hMKOZfftJ-0/maxresdefault.jpg,http://netvantagemarketing.com/blog/wp-content/uploads/2016/02/bed-and-breakfast-3.jpg,http://www.hirecarandgo.com/wp-content/uploads/2016/03/CarAndGoLogo3.png", "02:00:00",
				IllegalArgumentException.class
			}

		};

		for (int i = 0; i < testingData.length; i++)
			this.templateChangeBanner((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (Class<?>) testingData[i][3]);
	}
	protected void templateChangeBanner(final String username, final String banners, final String hours, final Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			this.authenticate(username);
			final Configuration configuration = this.configurationService.create();
			configuration.setBanners(banners);
			configuration.setSearchHours(hours);
			this.configurationService.save(configuration);
			this.unauthenticate();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}
}
