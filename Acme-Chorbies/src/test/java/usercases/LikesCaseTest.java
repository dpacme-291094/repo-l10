
package usercases;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import services.ChorbiService;
import services.LikesService;
import utilities.AbstractTest;
import domain.Chorbi;
import domain.Likes;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@Transactional
public class LikesCaseTest extends AbstractTest {

	@Autowired
	private LikesService	likesService;
	@Autowired
	private ChorbiService	chorbiService;


	//Requisito funcional: Like another chorbi;
	/*
	 * En el siguiente orden:
	 * Dar un like a un chorbi que no le he dado like (Correcto)
	 * Dar like a un chorbi que no existe(Fallo)
	 * Dar like logueado como admin(Fallo)
	 * Dar un like a un chorbi sin estar logueado (Fallo)
	 * 
	 * Dar like a un chorbi null(Fallo)
	 */
	@Test
	public void driverLike() {

		final Object testingData[][] = {

			{
				"chorbi1", 26, null
			}

			, {
				"chorbi1", 60, NullPointerException.class

			}, {
				"admin1", 26, IllegalArgumentException.class
			}, {
				null, 26, IllegalArgumentException.class
			}, {
				"chorbi1", null, InvalidDataAccessApiUsageException.class
			}

		};

		for (int i = 0; i < testingData.length; i++)
			this.templateLike((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);

	}
	//Requisito funcional:Cancel a like to another chorbi
	/*
	 * En el siguiente orden:
	 * Dar un dislike a un chorbi que le he dado like (Correcto)
	 * Dar dislike a un chorbi que no existe(Fallo)
	 * Dar dislike logueado como admin(Fallo)
	 * Dar un dislike a un chorbi sin estar logueado (Fallo)
	 * Dar dislike a un chorbi que no le he dado like(Fallo)
	 */
	@Test
	public void driverDislike() {

		final Object testingData[][] = {

			{
				"chorbi1", 26, null
			}

			, {
				"chorbi1", 60, NullPointerException.class

			}, {
				"admin1", 26, IllegalArgumentException.class
			}, {
				null, 26, IllegalArgumentException.class

			}, {
				"chorbi2", null, InvalidDataAccessApiUsageException.class
			}

		};

		for (int i = 0; i < testingData.length; i++)
			this.templateDislike((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);

	}
	protected void templateLike(final String username, final Integer chorbiId, final Class<?> expected) {

		Class<?> caught;

		caught = null;

		try {
			this.authenticate(username);
			final Chorbi c = this.chorbiService.findOne(chorbiId);
			final Likes k = this.likesService.save(this.likesService.create(c));
			this.likesService.like(c, k);
			this.unauthenticate();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}

	protected void templateDislike(final String username, final Integer chorbiId, final Class<?> expected) {

		Class<?> caught;

		caught = null;

		try {
			this.authenticate(username);
			final Chorbi c = this.chorbiService.findOne(chorbiId);
			final Likes k = this.likesService.save(this.likesService.create(c));
			this.likesService.like(c, k);
			this.likesService.dislike(k);
			this.unauthenticate();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}

}
