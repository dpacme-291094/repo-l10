
package usercases;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import services.ChorbiService;
import services.SearchTemplateService;
import utilities.AbstractTest;
import domain.Chorbi;
import domain.Coordinates;
import domain.SearchTemplate;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@Transactional
public class SearchCaseTest extends AbstractTest {

	@Autowired
	private ChorbiService			chorbiService;
	@Autowired
	private SearchTemplateService	searchTemplateService;


	/*
	 * Requisito funcional: Change his or her search template.
	 * Se comprueba en el siguiente orden:
	 * El Chorbi "chorbi2" edita su Search template introduciendolos correctos (correcto)
	 * "null" intenta modificar sus datos del Search template(erroneo)
	 */
	@Test
	public void driverEditSearchTemplate() {

		@SuppressWarnings("deprecation")
		final Object testingData[][] = {
			{
				"chorbi2", 18, "Sevilla", "man", "hola", "activities", null
			}, {
				null, 18, "Sevilla", "man", "hola", "activities", IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.templateEditSearchTemplate((String) testingData[i][0], (Integer) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5], (Class<?>) testingData[i][6]);
	}

	/*
	 * Requisito funcional: Browse the results of his or her search template
	 * Se comprueba en el siguiente orden:
	 * Listar los resultados registrado como chorbi(correcto)
	 * Listar la lista sin estar registrado(Fallo)
	 */
	@Test
	public void driverBrowseResultSearchTemplate() {

		final Object testingData[][] = {
			{
				"chorbi2", null
			}, {
				null, IllegalArgumentException.class
			}

		};

		for (int i = 0; i < testingData.length; i++)
			this.templateBrowseResultSearchTemplate((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	private void templateEditSearchTemplate(final String username, final Integer age, final String city, final String genre, final String keyword, final String relationship, final Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {

			this.authenticate(username);
			final Chorbi c = this.chorbiService.findByPrincipal();

			final SearchTemplate searchTemplate = c.getSearch();
			final SearchTemplate searchTemplateSave;
			searchTemplate.setAge(age);
			final Coordinates coordinates = searchTemplate.getCoordinates();
			coordinates.setCity(city);
			searchTemplate.setCoordinates(coordinates);
			searchTemplate.setGenre(genre);
			searchTemplate.setKeyword(keyword);
			searchTemplate.setRelationship(relationship);
			searchTemplateSave = this.searchTemplateService.save(searchTemplate);

			c.setSearch(searchTemplateSave);
			this.chorbiService.edit(c);
			this.chorbiService.flush();
			this.searchTemplateService.flush();

			this.unauthenticate();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}

	private void templateBrowseResultSearchTemplate(final String username, final Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			this.authenticate(username);
			final Chorbi c = this.chorbiService.findByPrincipal();
			final SearchTemplate searchTemplate = c.getSearch();
			searchTemplate.getChorbies();
			this.unauthenticate();

		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}
}
