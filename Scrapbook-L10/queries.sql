C queries

+A listing with the number of chorbies per country and city.

select c.coordinates.city,count(c) from Chorbi c where c.coordinates.city is not null group by c.coordinates.city;
select c.coordinates.country,count(c) from Chorbi c where c.coordinates.country is not null group by c.coordinates.country;

+The minimum, the maximum, and the average ages of the chorbies.

select min(floor(abs((sysdate()-c.birthDate)/(317.09999*365*60*60*24)+0.0002) * 100 + 0.5)/100.0 * sign((sysdate()-c.birthDate)/(317.09999*365*60*60*24)+0.0002)),max(floor(abs((sysdate()-c.birthDate)/(317.09999*365*60*60*24)+0.0002) * 100 + 0.5)/100.0 * sign((sysdate()-c.birthDate)/(317.09999*365*60*60*24)+0.0002)),floor(abs(avg(floor(abs((sysdate()-c.birthDate)/(317.09999*365*60*60*24)+0.0002) * 100 + 0.5)/100.0 * sign((sysdate()-c.birthDate)/(317.09999*365*60*60*24)+0.0002))) * 100 + 0.5)/100.0 * sign(avg(floor(abs((sysdate()-c.birthDate)/(317.09999*365*60*60*24)+0.0002) * 100 + 0.5)/100.0 * sign((sysdate()-c.birthDate)/(317.09999*365*60*60*24)+0.0002))) from Chorbi c;


+The ratio of chorbies who have not registered a credit card or have registered an invalid credit card.

select sum( Case When YEAR(sysdate())>c.creditCard.expirationYear or (YEAR(sysdate())=c.creditCard.expirationYear and MONTH(sysdate())>=c.creditCard.expirationMonth) Then 1 Else 0 End)*1.0/count(c) from Chorbi c;
select sum( Case When c.creditCard is null then 1 Else 0 end)*1.0/count(c) from Chorbi c;


+The ratios of chorbies who search for �activities�, �friendship�, and �love�.

select sum( Case When c.search.relationship='activities' then 1 Else 0 end)*1.0/count(c) from Chorbi c;
select sum( Case When c.search.relationship='friendship' then 1 Else 0 end)*1.0/count(c) from Chorbi c;
select sum( Case When c.search.relationship='love' then 1 Else 0 end)*1.0/count(c) from Chorbi c;


B Queries
+The list of chorbies, sorted by the number of likes they have got.

select c,c.likesReceived.size from Chorbi c order by c.likesReceived.size desc;

+The minimum, the maximum, and the average number of likes per chorbi.

select min(c.likesReceived.size),max(c.likesReceived.size),avg(c.likesReceived.size) from Chorbi c;


A Queries
+The minimum, the maximum, and the average number of chirps that a chorbi receives from other chorbies.

select min(c.chirpsReceived.size),max(c.chirpsReceived.size),avg(c.chirpsReceived.size) from Chorbi c;

+The minimum, the maximum, and the average number of chirps that a chorbi sends to other chorbies.

select min(c.chirpsSent.size),max(c.chirpsSent.size),avg(c.chirpsSent.size) from Chorbi c;

+The chorbies who have got more chirps.

select c,c.chirpsReceived.size from Chorbi c where c.chirpsReceived.size=(select max(c.chirpsReceived.size) from Chorbi c);


+The chorbies who have sent more chirps.

select c,c.chirpsSent.size from Chorbi c where c.chirpsSent.size=(select max(c.chirpsSent.size) from Chorbi c);
